package pl.codementors.zoo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

/**
 * Created by maryb on 07.06.2017.
 */

public class ZooMain {


    public static final String FILE = "animal.txt";
    public static final String FILE_BINARY = "animalBinary";


    public static void main(String[] args) {
        System.out.println("Hello to the ZOO");

        Scanner scanner = new Scanner(System.in);

        int size;

        while (true) {
            System.out.println("Added animals to Zoo:  ");
            String line = scanner.nextLine();

            if (line.matches("\\d+")) {
                size = Integer.parseInt(line);
                break;
            }
        }

        Animal[] animals = new Animal[size];
        System.out.println("You have " + size + " animals.");

        for (int i = 0; i < animals.length; i++) {
            boolean correctData = false;
            while (!correctData) {
                System.out.println("Get type of animal: ");
                String typeOfAnimal = scanner.nextLine();
                typeOfAnimal = typeOfAnimal.toLowerCase();

                switch (typeOfAnimal) {

                    case "wolf": {
                        Wolf wolf = new Wolf();
                        System.out.println("Get a name: ");
                        wolf.setName(scanner.nextLine());
                        System.out.println("Get a color: ");
                        wolf.setFurColor(scanner.nextLine());
                        animals[i] = wolf;
                        correctData = true;
                        break;
                    }

                    case "parrot": {
                        Parrot parrot = new Parrot();
                        System.out.println("Get a name: ");
                        parrot.setName(scanner.nextLine());
                        System.out.println("Get a color: ");
                        parrot.setFeatherColor(scanner.nextLine());
                        animals[i] = parrot;
                        correctData = true;
                        break;
                    }

                    case "iguana": {
                        Iguana iguana = new Iguana();
                        System.out.println("Get a name: ");
                        iguana.setName(scanner.nextLine());
                        System.out.println("Get a color: ");
                        iguana.setScaleColor(scanner.nextLine());
                        animals[i] = iguana;
                        correctData = true;
                        break;
                    }

                    default:
                        System.out.println("Wrong type of animal. Choose another one.");
                }
            }
        }
        print(animals);
        feed(animals);
        voiceOfAnimal(animals);
        feedAnimal(animals);
        printColor(animals);
        saveToFile(animals, FILE);
        readFromFile(animals, FILE);
        saveToBinaryFile(animals, FILE_BINARY);
        readFromBinaryFile(FILE_BINARY);
    }

    static void print(Animal[] bbbb) {
        for (Animal a : bbbb) {
            System.out.println(a.getClass().getSimpleName());
            System.out.println(a.getName());
        }
    }

    static void feed(Animal[] fff) {
        for (Animal f : fff) {
            f.eat();
        }
    }

    static void voiceOfAnimal(Animal[] voa) {
        for (Animal v : voa) {
            if (v instanceof Wolf) {
                ((Wolf) v).howl();
            } else if (v instanceof Iguana) {
                ((Iguana) v).hiss();
            } else if (v instanceof Parrot) {
                ((Parrot) v).screech();
            }
        }
    }

    static void feedAnimal(Animal[] fa) {
        for (Animal f : fa) {
            if (f instanceof Carnivorous) {
                ((Carnivorous) f).eatMeat();
            } else if (f instanceof Herbivorous) {
                ((Herbivorous) f).eatPlant();
            }
        }
    }

    static void printColor(Animal[] animals) {
        for (Animal animal : animals) {

            if (animal instanceof Wolf) {
                System.out.println(animal.getName());
                System.out.println(((Wolf) animal).getFurColor());
            } else if (animal instanceof Parrot) {
                System.out.println(animal.getName());
                System.out.println(((Parrot) animal).getFeatherColor());
            } else if (animal instanceof Iguana) {
                System.out.println(animal.getName());
                System.out.println(((Iguana) animal).getScaleColor());
            }
        }
    }

    static void saveToFile(Animal[] animals, String nameOfFile) {
        try (FileWriter fw = new FileWriter(nameOfFile);
             BufferedWriter bw = new BufferedWriter(fw);) {

            for (Animal a : animals) {

                bw.write(a.getName());
                bw.newLine();
                bw.write(a.getClass().getSimpleName());
                bw.newLine();
            }

        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    static void readFromFile(Animal[] animals, String nameOfFile) {
        try {
            FileReader fw = new FileReader("animal.txt");
            BufferedReader bf = new BufferedReader(fw);

            String string = null;

            while ((string = bf.readLine()) != null) {
                System.out.println(string);
            }
            bf.close();

        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    static void saveToBinaryFile(Animal[] animals, String nameOfFile) {
        try (FileOutputStream fos = new FileOutputStream(nameOfFile)) {
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            {
                oos.writeObject(animals);
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    static Animal[] readFromBinaryFile(String nameOfFile) {
        try (FileInputStream fis = new FileInputStream("animalBinary");
             ObjectInputStream ois = new ObjectInputStream(fis);) {

            return (Animal[]) ois.readObject();

        } catch (IOException | ClassNotFoundException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
        return new Animal[0];
    }
}