package pl.codementors.zoo;

/**
 * Created by maryb on 07.06.2017.
 */
public abstract class Bird extends Animal {

    private String featherColor;

    public String getFeatherColor() {
        return featherColor;
    }

    public void setFeatherColor(String featherColor) {
        this.featherColor = featherColor;
    }
}
