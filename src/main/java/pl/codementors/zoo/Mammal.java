package pl.codementors.zoo;

/**
 * Created by maryb on 07.06.2017.
 */
public abstract class Mammal extends Animal {

    private String furColor;

    public String getFurColor() {
        return furColor;
    }

    public void setFurColor(String furColor) {
        this.furColor = furColor;
    }
}
