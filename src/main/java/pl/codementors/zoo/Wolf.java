package pl.codementors.zoo;

/**
 * Created by maryb on 07.06.2017.
 */
public class Wolf extends Mammal implements Carnivorous {

    public void howl(){
        System.out.println(getName() + " howls");
    }

    @Override
    public void eat() {
        System.out.println(getName() + " eats");
    }

    @Override
    public void eatMeat() {
        System.out.println("It eats meat.");
    }
}
