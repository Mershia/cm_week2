package pl.codementors.zoo;

/**
 * Created by maryb on 09.06.2017.
 */
public interface Herbivorous {

    void eatPlant();
}
