package pl.codementors.zoo;

/**
 * Created by maryb on 07.06.2017.
 */
public abstract class Lizard extends Animal {

    private String scaleColor;

    public String getScaleColor() {
        return scaleColor;
    }

    public void setScaleColor(String scaleColor) {
        this.scaleColor = scaleColor;
    }
}
