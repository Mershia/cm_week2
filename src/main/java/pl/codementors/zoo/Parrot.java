package pl.codementors.zoo;


/**
 * Created by maryb on 07.06.2017.
 */
public class Parrot extends Bird implements Herbivorous{

    public void screech(){
        System.out.println(getName() + " screeches");
    }

    @Override
    public void eat() {
        System.out.println(getName() + " eats");
    }

    @Override
    public void eatPlant() {
        System.out.println("It eats nuts.");
    }
}
