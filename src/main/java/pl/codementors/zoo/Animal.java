package pl.codementors.zoo;

import java.io.Serializable;

/**
 * Created by maryb on 07.06.2017.
 */
public abstract class Animal implements Serializable{

    private String name;

    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public abstract void eat();
}
