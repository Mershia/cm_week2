package pl.codementors.zoo;

/**
 * Created by maryb on 07.06.2017.
 */
public class Iguana extends Lizard implements Herbivorous {

    public void hiss(){
        System.out.println(getName() + " hisses");
    }

    @Override
    public void eat() {
        System.out.println(getName() + " eats");
    }

    @Override
    public void eatPlant() {
        System.out.println("It eats leaves.");
    }


}
